package edu.server.controllers

import edu.server.services.ZipUtils
import org.apache.tomcat.util.http.fileupload.IOUtils
import org.springframework.web.bind.annotation.PathVariable
import java.io.FileInputStream
import javax.servlet.http.HttpServletResponse

fun getZipDirectory(response: HttpServletResponse, @PathVariable("directory") directory: String) {
    try {
        response.contentType = "application/force-download"
        response.setHeader("Content-Disposition", "attachment; filename=" + "directory.zip")
        ZipUtils.zip(directory, response.outputStream)
        response.flushBuffer()
    } catch (e: Exception) {
        println(e.message)
    }

}

fun getZipFile(response: HttpServletResponse, @PathVariable file: String, @PathVariable type: String, directory: String) {
    try {
        response.contentType = "application/force-download"
        response.setHeader("Content-Disposition", "attachment; filename=$file.zip")
        val path = "$directory$file.$type"
        ZipUtils.zip(path, response.outputStream)
        response.flushBuffer()
    } catch (e: Exception) {
        println(e.message)
    }

}

fun getFile(response: HttpServletResponse, @PathVariable file: String, @PathVariable type: String, directory: String) {
    try {
        response.contentType = "application/force-download"
        response.setHeader("Content-Disposition", "attachment; filename=$file.$type")
        FileInputStream(directory + file + "." + type).use { fileInputStream -> IOUtils.copy(fileInputStream, response.outputStream) }
        response.flushBuffer()
    } catch (e: Exception) {
        println(e.message)
    }

}
