package edu.server.controllers

import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletResponse

@RestController
class CatalogController {
    @RequestMapping(value = "/", method = arrayOf(RequestMethod.GET))
    @ResponseBody
    fun getTime() = Date().toString()

    @RequestMapping(value = "/directory/{directory}", method = arrayOf(RequestMethod.GET))
    fun getLogFile(response: HttpServletResponse,
                   @PathVariable("directory") directory: String
    ) = getZipDirectory(response, "C:\\" + (directory.replace(">", "\\") + "\\"))


    @RequestMapping(value = "/zip/{file}/{type}", method = arrayOf(RequestMethod.GET))
    @Throws(Exception::class)
    fun getLogFile4(response: HttpServletResponse,
                    @PathVariable file: String,
                    @PathVariable type: String
    ) = getZipFile(response, file, type, "C:\\")


    @RequestMapping(value = "/zip/{directory}/{file}/{type}", method = arrayOf(RequestMethod.GET))
    @Throws(Exception::class)
    fun getLogFile2(response: HttpServletResponse,
                    @PathVariable("directory") directory: String,
                    @PathVariable file: String,
                    @PathVariable type: String
    ) = getZipFile(response, file, type, "C:\\" + (directory.replace(">", "\\") + "\\"))


    @RequestMapping(value = "/file/{file}/{type}", method = arrayOf(RequestMethod.GET))
    @Throws(Exception::class)
    fun getLogFile5(response: HttpServletResponse,
                    @PathVariable file: String,
                    @PathVariable type: String
    ) = getFile(response, file, type, "C:\\")

    @RequestMapping(value = "/file/{directory}/{file}/{type}", method = arrayOf(RequestMethod.GET))
    @Throws(Exception::class)
    fun getLogFile3(response: HttpServletResponse,
                    @PathVariable("directory") directory: String,
                    @PathVariable file: String,
                    @PathVariable type: String
    ) = getFile(response, file, type, "C:\\" + (directory.replace(">", "\\") + "\\"))
}
