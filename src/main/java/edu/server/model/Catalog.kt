package edu.server.model

import java.io.File
import java.util.*

class Catalog(val name: String, val type: Type, val level: Int = 0, val file: File, val parent: Catalog?) {

    val childes = ArrayList<Catalog>()
    fun addChild(name: String, type: Type, level: Int, file: File): Catalog {
        val element = Catalog(name, type, level, file, this)
        childes.add(element)
        return element;
    }
}