package edu.server.model


enum class Type {
    File, Directory
}