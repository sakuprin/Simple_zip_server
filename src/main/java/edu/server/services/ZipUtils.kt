package edu.server.services

import edu.server.model.Catalog
import edu.server.model.Type
import java.io.File
import java.io.FileNotFoundException
import java.io.OutputStream
import java.nio.file.Paths
import java.util.*


object ZipUtils {
    private val generator = ZipGenerator()
    fun zip(path: String, outputStream: OutputStream) {
        val rootFile = Paths.get(path).toFile()
        if (!rootFile.exists()) throw FileNotFoundException("$path not found")
        val catalog = Catalog(rootFile.name, getType(rootFile), 0, rootFile, null)
        rootFile.listFiles()?.forEach { generateCatalog(it, catalog) }
        generator.zipCatalog(catalog, outputStream)
    }

    fun generateCatalog(file: File, catalog: Catalog) {
        if ( catalog.type == Type.File) return;
        val childCatalog = catalog.addChild(file.name, if (file.isDirectory) Type.Directory else Type.File, catalog.level + 1, file)
        file.listFiles()?.forEach { generateCatalog(it, childCatalog) }
    }

    fun generateFullFileName(catalog: Catalog?): String {
        val stringDeque = ArrayDeque<String>()
        var currentCatalog = catalog
        while (currentCatalog != null) {
            val name = currentCatalog.file.name
            stringDeque.push(name)
            currentCatalog = currentCatalog.parent
        }
        val builder = StringBuilder()
        while (stringDeque.size > 0) {
            builder.append(stringDeque.pop())
            if (!stringDeque.isEmpty()) builder.append(File.separator)
        }
        return builder.toString()
    }

    fun getType(rootFile: File) = if (rootFile.isDirectory) Type.Directory else Type.File
}

