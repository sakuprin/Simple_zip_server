package edu.server.services


import edu.server.model.Catalog
import edu.server.model.Type
import org.apache.tomcat.util.http.fileupload.IOUtils
import java.io.FileInputStream
import java.io.IOException
import java.io.OutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class ZipGenerator {
    fun zipCatalog(catalog: Catalog, fos: OutputStream) {
        try {
            val zipOutputStream = ZipOutputStream(fos)
            packDirectory(catalog, zipOutputStream)
            zipOutputStream.closeEntry()
            zipOutputStream.close()
        } catch (ex: IOException) {
            println(ex.message)
        }
    }

    private fun packDirectory(file: Catalog, zipOutputStream: ZipOutputStream) {
        if (file.type === Type.File) addFile(file, zipOutputStream)
        else file.childes.forEach { it -> packDirectory(it, zipOutputStream) }
    }


    private fun addFile(catalog: Catalog, zipOutputStream: ZipOutputStream) {
        try {
            val zipEntry = ZipEntry(ZipUtils.generateFullFileName(catalog))
            zipOutputStream.putNextEntry(zipEntry)
            IOUtils.copy(FileInputStream(catalog.file), zipOutputStream)
        } catch (e: IOException) {
            println(e.message)
        }
    }
}